<?= view('layouts/header'); ?>

<div class="appHeader no-border transparent position-absolute">
    <div class="left">
        <a href="<?= base_url("auth") ?>" class="headerButton goBack">
            <ion-icon name="chevron-back-outline"></ion-icon>
        </a>
    </div>
    <div class="pageTitle"></div>
    <div class="right">
    </div>
</div>

<div id="appCapsule">
    <div class="login-form" style="padding-top: 80px;">
        <div class="section">
            <h2>Request OTP</h2>
            <h5>Input Your OTP</h5>
        </div>
        <div class="section mt-2 mb-5">
            <form>

                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <input type="text" class="form-control verify-input" id="code" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" placeholder="••••••" maxlength="6">
                    </div>
                </div>

                <div class="btn-otps" style="padding-top: 20px;">
                    <button type="button" onclick="SendOTP()" id="btnOtp" class="btn btn-primary rounded me-1">Verify</button>
                </div>

            </form>
        </div>
    </div>
</div>

<?= view('layouts/modal'); ?>
<?= view('layouts/script'); ?>
<?= view('js/otp'); ?>
<?= view('js/auth'); ?>
<?= view('modal/popup'); ?>