<?= view('layouts/header'); ?>

<div id="appCapsule" class="pt-0">

    <div class="login-form mt-1">

        <div class="heading-login" style="padding-top: 58px;">
            <h2>Register</h2>
        </div>

        <div class="section mt-1 mb-5" style="padding-top: 10px;">
            <form>
                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <input type="text" class="form-control icon-rtl" id="fullname" placeholder="Fullname" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');">
                        <i class="right-inner" style="top: 88px;">
                            <ion-icon name="person-circle-outline"></ion-icon>
                        </i>
                    </div>
                </div>

                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <input type="text" class="form-control icon-rtl" id="phone" placeholder="Phone Number" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');">
                        <i class="right-inner" style="top: -3px;font-size: 20px;">
                            <ion-icon name="call-outline"></ion-icon>
                        </i>
                    </div>
                </div>

                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <input type="email" class="form-control icon-rtl" id="email" placeholder="E-mail Address" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');">
                        <i class="right-inner2">
                            <ion-icon name="mail-outline"></ion-icon>
                        </i>
                    </div>
                </div>

                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <input type="text" class="form-control icon-rtl" id="carId" placeholder="Car ID" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');">
                        <i class="right-inner2">
                            <ion-icon name="car-outline"></ion-icon>
                        </i>
                    </div>
                </div>

                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <input type="text" class="form-control icon-rtl" id="identityNumber" placeholder="Identity Number" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');">
                        <i class="right-inner2">
                            <ion-icon name="card-outline"></ion-icon>
                        </i>
                    </div>
                </div>

                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <input type="password" class="form-control" id="password" placeholder="Password" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');">
                        <i class="right-inner2">
                            <ion-icon name="lock-closed-outline"></ion-icon>
                        </i>
                    </div>
                </div>

                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <input type="password" class="form-control" id="confirmPassword" placeholder="Confirm Password" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');">
                        <i class="right-inner2">
                            <ion-icon name="lock-closed-outline"></ion-icon>
                        </i>
                    </div>
                </div>

                <div class="btn-login">
                    <a type="button" onclick="Register()" id="register" class="btn btn-primary rounded me-1">REGISTER</a>
                </div>
        </div>

        </form>
    </div>
</div>
</div>

<?= view('layouts/modal'); ?>
<?= view('layouts/script'); ?>
<?= view('js/auth'); ?>
<?= view('modal/popup'); ?>