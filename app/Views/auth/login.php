<?= view('layouts/header'); ?>

<div id="appCapsule" class="pt-0">

  <div class="login-form mt-1">

    <div class="heading-reset">
      <h5>Havent registered yet? <a style="color: blue;" type="button" id="resetUser"> Click here.</a></h5>
    </div>

    <div class="section mt-1 mb-5" style="padding-top: 10px;">
      <form>
        <div class="form-group boxed">
          <div class="input-wrapper">
            <input type="text" class="form-control icon-rtl" id="username" name="username" placeholder="Username" style="font-size: 12px;" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');">
            <i class="right-inner" style="top: -4px;">
              <ion-icon name="person-circle-outline"></ion-icon>
            </i>
          </div>
        </div>

        <div class="form-group boxed">
          <div class="input-wrapper">
            <input type="password" class="form-control" id="password" placeholder="Password" autocomplete="off" style="font-size: 12px;" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');">
            <i class="right-inner2">
              <ion-icon name="lock-closed-outline"></ion-icon>
            </i>
          </div>
        </div>

        <div class="form-group boxed" style="margin-left: -57px;font-size: 12px;margin-top: -12px;">
          <span>Forgot password ?</span><a href="<?= base_url("auth/forgot-password") ?>"> Click here.</a>
        </div>

        <div class="btn-login">
          <a type="button" onclick="Login()" id="login" class="btn btn-primary rounded me-1">LOGIN</a>
        </div>
    </div>

    </form>
  </div>
</div>
</div>

<?= view('layouts/modal'); ?>
<?= view('layouts/script'); ?>
<?= view('js/auth'); ?>
<?= view('modal/popup'); ?>