<?= view('layouts/header'); ?>

<div id="appCapsule" class="pt-0">

  <div class="login-form mt-1">

    <div class="section mt-1 mb-5" style="padding-top: 80px;">
    <form  autocomplete="off">
        <div class="form-group boxed">
          <div class="input-wrapper">
            <input type="text" class="form-control icon-rtl" id="newPassword" placeholder="new password" style="font-size: 12px;" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');">
            <i class="right-inner" style="top: 0px;">
              <ion-icon name="person-circle-outline"></ion-icon>
            </i>
          </div>
        </div>

        <div class="form-group boxed">
          <div class="input-wrapper">
            <input type="text" class="form-control" id="confirmPassword" placeholder="confirm password " autocomplete="off" style="font-size: 12px;" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');">
            <i class="right-inner2">
              <ion-icon name="lock-closed-outline"></ion-icon>
            </i>
          </div>
        </div>

        <div class="btn-login">
          <a type="button" onclick="SubmitNewPassword()" id="login" class="btn btn-primary rounded me-1">Submit</a>
        </div>
    </div>

    </form>
  </div>
</div>
</div>

<?= view('layouts/modal'); ?>
<?= view('layouts/script'); ?>
<?= view('js/auth'); ?>
<?= view('modal/popup'); ?>