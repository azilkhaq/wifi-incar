<?= view('layouts/header'); ?>

<div id="appCapsule">
    <div class="login-form" style="padding-top: 80px;">
        <div class="section">
            <h5>Enter the OTP sent to your e-mail</h5>
        </div>
        <div class="section mt-2 mb-5">
            <form>

                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <input type="text" class="form-control verify-input" id="code" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" placeholder="••••••" maxlength="6" style="max-width: 250px;">
                    </div>
                    <div class="resend">
                        <span>Don't receive the OTP? <a href="">Resend here</a></span>
                    </div>
                    <div class="cooldown">
                        <span>00:48</span>
                    </div>
                </div>
            </form>
        </div>
        <div class="btn-otp">
            <a href="<?= base_url("auth/forgot-password") ?>" id="reset" class="btn btn rounded me-1">Back</a>
            <a href="<?= base_url("auth/forgot-password/new") ?>" id="reset" class="btn btn-primary rounded me-1">Submit</a>
        </div>
    </div>
</div>

<?= view('layouts/modal'); ?>
<?= view('layouts/script'); ?>
<?= view('js/otp'); ?>
<?= view('js/auth'); ?>
<?= view('modal/popup'); ?>