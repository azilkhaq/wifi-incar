<?= view('layouts/header'); ?>

<body class="body-home">
    <div id="appCapsule">

        <div class="head-welcome">
            <div class="welcome">
                <img src="<?= base_url("public/assets/img/icon.svg") ?>" alt="image" class="image-people">
                <div style="margin-top: 8px;">
                    <span>Welcome, Kristanto</span>
                </div>
            </div>
        </div>

        <div class="chart-div">
            <div class="canvas">
                <canvas id="myChart" width="100" height="100"></canvas>
                <img src="<?= base_url("public/assets/img/icon-chart.svg") ?>" alt="image" class="image-chart">
            </div>
            <div class="desc-canvas">
                <p class="desc-chart">7.21 GB <span class="sp-chart"> / 10 GB</span> </p>
                <p class="desc-chart-2">until 31 May 2021 23:59:59</span> </p>
            </div>
        </div>


        <div class="menu-home">
            <div class="card card-menu-home">
                <div class="card-body">
                    <div class="content">

                        <div class="top-menu">
                            <div class="card card-top-menu">
                                <div class="card-body">
                                    <div style="display: flex;">
                                        <a class="btn btn-menu-icon"><img src="<?= base_url("public/assets/img/icon1.svg") ?>" alt="image"></a><span class="span-menu-icon">Connectivity</span>&emsp;
                                        <a href="<?= base_url("product") ?>" class="btn btn-menu-icon"><img src="<?= base_url("public/assets/img/icon2.svg") ?>" alt="image"></a><span class="span-menu-icon2">Purchase</span>&emsp;
                                        <a href="<?= base_url("support") ?>" class="btn btn-menu-icon"><img src="<?= base_url("public/assets/img/support.svg") ?>" style="width: 30px; height:31px;" alt="image"></a><span class="span-menu-icon3">Support</span>&emsp;
                                        <a href="<?=  base_url("profile") ?>" class="btn btn-menu-icon"><img src="<?= base_url("public/assets/img/icon3.svg") ?>" alt="image"></a><span class="span-menu-icon4">Profile</span>&emsp;
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="history">
                            <h2>History</h2>

                            <div class="top-history">
                                <div class="card card-top-history">
                                    <div class="card-body" style="padding: 12px 15px;">
                                        <div style="display: flex;">
                                            <div class="button-history">
                                                <a href="#" class="btn btn-primary rounded me-1">Buy Again</a>
                                            </div>
                                            <div class="description">
                                                <p class="desc-history desc-bold">10 GB Data / 30 Days</p>
                                                <p class="desc-history text-muted">on 1 May 2021 14:57</p>
                                            </div>
                                        </div>
                                    </div>
                                </div><br>

                                <div class="card card-top-history">
                                    <div class="card-body" style="padding: 12px 15px;">
                                        <div style="display: flex;">
                                            <div class="button-history">
                                                <a href="#" class="btn btn-primary rounded me-1">Buy Again</a>
                                            </div>
                                            <div class="description">
                                                <p class="desc-history desc-bold">10 GB Data / 30 Days</p>
                                                <p class="desc-history text-muted">on 7 April 2021 11:39</p>
                                            </div>
                                        </div>
                                    </div>
                                </div><br>

                                <div class="card card-top-history">
                                    <div class="card-body" style="padding: 12px 15px;">
                                        <div style="display: flex;">
                                            <div class="button-history">
                                                <a href="#" class="btn btn-primary rounded me-1">Buy Again</a>
                                            </div>
                                            <div class="description">
                                                <p class="desc-history desc-bold">10 GB Data / 30 Days</p>
                                                <p class="desc-history text-muted">on 1 April 2021 12:40</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><br>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

<?= view('layouts/modal'); ?>
<?= view('layouts/script'); ?>
<?= view('js/home'); ?>

