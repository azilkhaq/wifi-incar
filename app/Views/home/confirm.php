<?= view('layouts/header'); ?>


<style>
    .desc-confirm {
        position: absolute;
        font-size: 15px;
        margin-top: 10px;
    }
</style>

<body>

    <div class="appHeader no-border transparent position-absolute">
        <div class="left">
            <a href="<?= base_url("product") ?>" class="headerButton goBack">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle"></div>
        <div class="right">
        </div>
    </div>

    <div id="appCapsule">

        <div class="menu-product">
            <div class="content-product">

                <div class="product">
                    <h4 style="padding-bottom: 10px;"><?= $product->description->name ?></h4>
                    <h4>Which Payment do you choose:</h4>

                    <div class="top-product" style="padding-bottom: 180px; overflow-y: auto;">

                        <div class="card card-top-history" onclick="ShowConfirm('<?= $product->productId ?>','<?= $product->description->name ?>','shopeepay')">
                            <div class="card-body" style="padding: 12px 15px;">
                                <div style="display: flex;">
                                    <div class="button-history">
                                        <img src="<?= base_url("public/assets/img/shopeepay.png") ?>" alt="image" width="80">
                                    </div>
                                    <div class="description">
                                        <span class="desc-confirm desc-bold">Shoope Pay</span>
                                    </div>
                                </div>
                            </div>
                        </div><br>

                        <div class="card card-top-history" onclick="ShowConfirm('<?= $product->productId ?>','<?= $product->description->name ?>','gopay')">
                            <div class="card-body" style="padding: 12px 15px;">
                                <div style="display: flex;">
                                    <div class="button-history">
                                        <img src="<?= base_url("public/assets/img/gopay.png") ?>" alt="image" width="80">
                                    </div>
                                    <div class="description">
                                        <span class="desc-confirm desc-bold">Go-Pay</span>
                                    </div>
                                </div>
                            </div>
                        </div><br>

                        <div class="card card-top-history" onclick="ShowConfirm('<?= $product->productId ?>','<?= $product->description->name ?>','akulaku')">
                            <div class="card-body" style="padding: 12px 15px;">
                                <div style="display: flex;">
                                    <div class="button-history">
                                        <img src="<?= base_url("public/assets/img/akulaku.png") ?>" alt="image" style="width: 90px; margin-right:-19px">
                                    </div>
                                    <div class="description">
                                        <span class="desc-confirm desc-bold">Akulaku</span>
                                    </div>
                                </div>
                            </div>
                        </div><br>

                        <div class="card card-top-history" onclick="ShowConfirm('<?= $product->productId ?>','<?= $product->description->name ?>','dana')">
                            <div class="card-body" style="padding: 12px 15px;">
                                <div style="display: flex;">
                                    <div class="button-history">
                                        <img src="<?= base_url("public/assets/img/dana.png") ?>" alt="image" width="80" style="padding-top: 10px;padding-bottom: 10px;margin-right: 5px;">
                                    </div>
                                    <div class="description">
                                        <span class="desc-confirm desc-bold">Dana</span>
                                    </div>
                                </div>
                            </div>
                        </div><br>

                        <div class="card card-top-history" onclick="ShowConfirm('<?= $product->productId ?>','<?= $product->description->name ?>','indomaret')">
                            <div class="card-body" style="padding: 12px 15px;">
                                <div style="display: flex;">
                                    <div class="button-history">
                                        <img src="<?= base_url("public/assets/img/indomaret.png") ?>" alt="image" width="70" style="padding-top: 4px; padding-bottom: 4px;">
                                    </div>
                                    <div class="description">
                                        <span class="desc-confirm desc-bold">Indomaret</span>
                                    </div>
                                </div>
                            </div>
                        </div><br>

                        <div class="card card-top-history" onclick="ShowConfirm('<?= $product->productId ?>','<?= $product->description->name ?>','kredivo')">
                            <div class="card-body" style="padding: 12px 15px;">
                                <div style="display: flex;">
                                    <div class="button-history">
                                        <img src="<?= base_url("public/assets/img/kredivo.png") ?>" alt="image" width="70" style="padding-top: 8px; padding-bottom: 8px;">
                                    </div>
                                    <div class="description">
                                        <span class="desc-confirm desc-bold">Kredivo</span>
                                    </div>
                                </div>
                            </div>
                        </div><br>

                        <div class="card card-top-history" onclick="ShowConfirm('<?= $product->productId ?>','<?= $product->description->name ?>','ovo')">
                            <div class="card-body" style="padding: 12px 15px;">
                                <div style="display: flex;">
                                    <div class="button-history">
                                        <img src="<?= base_url("public/assets/img/ovo.png") ?>" alt="image" width="70" style="padding-top: 4px; padding-bottom: 4px;">
                                    </div>
                                    <div class="description">
                                        <span class="desc-confirm desc-bold">OVO</span>
                                    </div>
                                </div>
                            </div>
                        </div><br>

                        <div class="card card-top-history" onclick="ShowConfirm('<?= $product->productId ?>','<?= $product->description->name ?>','linkaja')">
                            <div class="card-body" style="padding: 12px 15px;">
                                <div style="display: flex;">
                                    <div class="button-history">
                                        <img src="<?= base_url("public/assets/img/link_aja.png") ?>" alt="image" width="60">
                                    </div>
                                    <div class="description">
                                        <span class="desc-confirm desc-bold">Link Aja</span>
                                    </div>
                                </div>
                            </div>
                        </div><br>

                        <div class="card card-top-history" onclick="ShowConfirm('<?= $product->productId ?>','<?= $product->description->name ?>','alfamart')">
                            <div class="card-body" style="padding: 12px 15px;">
                                <div style="display: flex;">
                                    <div class="button-history">
                                        <img src="<?= base_url("public/assets/img/alfamart.png") ?>" alt="image" width="70" style="padding-top: 8px; padding-bottom: 8px;">
                                    </div>
                                    <div class="description">
                                        <span class="desc-confirm desc-bold">Alfamart</span>
                                    </div>
                                </div>
                            </div>
                        </div><br>

                        <div class="card card-top-history" onclick="ShowConfirm('<?= $product->productId ?>','<?= $product->description->name ?>','qris')">
                            <div class="card-body" style="padding: 12px 15px;">
                                <div style="display: flex;">
                                    <div class="button-history">
                                        <img src="<?= base_url("public/assets/img/qris.png") ?>" alt="image" width="75">
                                    </div>
                                    <div class="description">
                                        <span class="desc-confirm desc-bold">Qris</span>
                                    </div>
                                </div>
                            </div>
                        </div><br>

                    </div>

                </div>
            </div>
        </div>

        <div class="appBottomMenu infinite-menu">
            <div class="menu-home">
                <div class="card card-menu-home">
                    <div class="card-body">
                        <div class="content">

                            <div class="top-menu">
                                <div class="card card-top-menu">
                                    <div class="card-body">
                                        <div style="display: flex;">
                                            <a class="btn btn-menu-icon"><img src="<?= base_url("public/assets/img/icon1.svg") ?>" alt="image"></a><span class="span-menu-icon">Connectivity</span>&emsp;
                                            <a href="<?= base_url("product") ?>" class="btn btn-menu-icon"><img src="<?= base_url("public/assets/img/icon2.svg") ?>" alt="image"></a><span class="span-menu-icon2">Purchase</span>&emsp;
                                            <a class="btn btn-menu-icon"><img src="<?= base_url("public/assets/img/support.svg") ?>" style="width: 30px; height:31px;" alt="image"></a><span class="span-menu-icon3">Support</span>&emsp;
                                            <a class="btn btn-menu-icon"><img src="<?= base_url("public/assets/img/icon3.svg") ?>" alt="image"></a><span class="span-menu-icon4">Profile</span>&emsp;
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

<?= view('layouts/script'); ?>
<?= view('layouts/modal'); ?>

<script>
    ShowConfirm = (productId, productName, provider) => {
        $('#provider').val(provider);
        $('#productId').val(productId);
        $('#productName').text(productName);
        $('#confirmModal').modal('show');
    }

    NextConfirm = () => {
        let data = new FormData();
        var productId = $('#productId').val();
        var provider = $('#provider').val();

        $('#confirmModal').modal('hide');

        data.append('productId', productId);
        data.append('provider', provider);

        $.ajax({
            type: "POST",
            url: `${baseUrl}/product/payment`,
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(response) {

                var res = JSON.parse(response);

                if (res.status == false) {
                    $('#paymentFailed').modal('show');
                } else {
                    $('#messageSuccess').modal('show');

                    setTimeout(function() {
                        location.href = `${baseUrl}/home`;
                    }, 3000);
                }
            },
            error: function(err) {
                $('#paymentFailed').modal('show');
            }
        });
    }
</script>