<?= view('layouts/header'); ?>

<body>

    <div class="appHeader no-border transparent position-absolute">
        <div class="left">
            <a href="<?= base_url("home") ?>" class="headerButton goBack">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle"></div>
        <div class="right"></div>
    </div>

    <div id="appCapsule">

        <div class="menu-product">
            <div class="content-product">

                <div class="product" style="padding-bottom: 211px;">

                    <h4>Select Category</h4>

                    <div style="display: flex; padding-bottom: 30px;">
                        <div class="card card-top-history" style="width: 145px;border-radius: 10px;background: #ce1818; color:white;">
                            <div class="card-body" style="padding: 12px 15px;">
                                <div class="description">
                                    <i style="font-size: 20px;">
                                        <ion-icon name="wifi-outline"></ion-icon>
                                    </i>
                                    <p class="desc-history">Wi-Fi Related</p>
                                </div>
                            </div>
                        </div> &emsp;

                        <div class="card card-top-history" style="width: 145px;border-radius: 10px;background: #ff9e9e;">
                            <div class="card-body" style="padding: 12px 15px;">
                                <div style="display: flex;">
                                    <div class="description">
                                        <i style="font-size: 20px;">
                                            <ion-icon name="bag-handle-outline"></ion-icon>
                                        </i>
                                        <p class="desc-history">Purchase Related</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h4>Frequently Asked Questions - Wifi</h4>

                    <div style="padding-top: 15px;">
                        <h5> My Wi-fi is disabled</h5>
                        <hr>
                    </div>

                    <div>
                        <h5> My Wi-fi run slowly</h5>
                        <hr>
                    </div>

                    <div style="padding-top: 20px;">
                        <h4>Still have a question?</h4>

                        <h6><i style="font-size: 20px;">
                                <ion-icon name="call-outline"></ion-icon>
                            </i>&emsp; 188 (from Telkomsel number) / 0807 1 811 811</h6>
                        <h6><i style="font-size: 20px;">
                                <ion-icon name="mail-outline"></ion-icon>
                            </i>&emsp; cs@telkomsel.co.id</h6>
                    </div>

                    <div style="padding-top: 20px;">
                        <h4>Question regarding GAS / Wi-Fi?</h4>

                        <h6><i style="font-size: 20px;">
                                <ion-icon name="call-outline"></ion-icon>
                            </i>&emsp; honda-call-center</h6>
                        <h6><i style="font-size: 20px;">
                                <ion-icon name="mail-outline"></ion-icon>
                            </i>&emsp; honda-email@mail.com</h6>
                    </div>
                </div>
            </div>
        </div>

        <?= view('layouts/bottomMenu'); ?>

    </div>

</body>

<?= view('layouts/script'); ?>