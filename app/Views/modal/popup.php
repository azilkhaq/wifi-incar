<div id="errorMessage" class="notification-box">
    <div class="notification-dialog ios-style bg-danger">
        <div class="notification-header">
            <div class="in"></div>
            <div class="right">
                <a type="button" onclick="ClosePopup()" class="close-button">
                    <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                </a>
            </div>
        </div>
        <div class="notification-content">
            <div class="in">
                <h3 class="subtitle">Error</h3>
                <div class="text">
                    <span id="message"></span>
                </div>
            </div>
        </div>
    </div>
</div>