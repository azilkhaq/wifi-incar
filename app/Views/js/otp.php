<script>
    SendOTP = async () => {
        let data = new FormData();
        var code = $("#code").val();

        if (code == "") {
            $('#message').text('OTP cannot be empty');
            notification('errorMessage');
            return
        }

        data.append('code', code);

        $("#btnOtp").text('Loading...');

        await $.ajax({
            type: "POST",
            url: `${baseUrl}/auth/otp/post`,
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(response) {

                var res = JSON.parse(response);

                if (res.status == false) {
                    $('#message').text(res.message);
                    notification('errorMessage');
                } else {
                    location.href = `${baseUrl}/home`;
                }
            },
            error: function(err) {
                $("#btnOtp").text('Verify');
            }
        });
    }
</script>