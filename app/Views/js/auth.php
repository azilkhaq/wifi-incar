<script>
    ClosePopup = () => {
        $("#errorMessage").removeClass("show")
    }

    Register = async () => {
        let data = new FormData();
        var fullname = $("#fullname").val();
        var phone = $("#phone").val();
        var email = $("#email").val();
        var carId = $("#carId").val();
        var identityNumber = $("#identityNumber").val();
        let password = $("#password").val();
        let confirmPassword = $("#confirmPassword").val();

        if (fullname == "") {
            $('#message').text('Fullname cannot be empty');
            notification('errorMessage');
            return
        }

        if (phone == "") {
            $('#message').text('Phone Number cannot be empty');
            notification('errorMessage');
            return
        }

        if (email == "") {
            $('#message').text('Email cannot be empty');
            notification('errorMessage');
            return
        }

        if (carId == "") {
            $('#message').text('CarId cannot be empty');
            notification('errorMessage');
            return
        }

        if (identityNumber == "") {
            $('#message').text('Identity number cannot be empty');
            notification('errorMessage');
            return
        }

        if (password == "") {
            $('#message').text('Password cannot be empty');
            notification('errorMessage');
            return
        }

        if (confirmPassword != password) {
            $('#message').text('Confirm password does not match');
            notification('errorMessage');
            return
        }

        data.append('fullname', fullname);
        data.append('phone', phone);
        data.append('email', email);
        data.append('carId', carId);
        data.append('identityNumber', identityNumber);
        data.append('password', password);

        $("#register").text('Loading...');

        await $.ajax({
            type: "POST",
            url: `${baseUrl}/auth/register/post`,
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(response) {

                $('#registerSuccess').modal('show');

                setTimeout(function() {
                    location.href = `${baseUrl}`;
                }, 3000);
            },
            error: function(err) {

                $("#register").text('Sign Up');

                $('#message').text('Something went wrong please try again');
                notification('errorMessage');
            }
        });
    }


    Login = async () => {
        let data = new FormData();
        var username = $("#username").val();
        let password = $("#password").val();

        if (username == "") {
            $('#message').text('Username cannot be empty');
            notification('errorMessage');
            return
        }

        if (password == "") {
            $('#message').text('Password cannot be empty');
            notification('errorMessage');
            return
        }

        data.append('username', username);
        data.append('password', password);

        $("#login").text('Loading...');

        await $.ajax({
            type: "POST",
            url: `${baseUrl}/auth/login/post`,
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(response) {

                location.href = `${baseUrl}/otp`;
            },
            error: function(err) {

                $("#login").text('Sign In');

                $('#loginMessage').modal('show');
            }
        });
    }

    SubmitNewPassword = () => {
        $('#messageSuccessReset').modal('show');

        setTimeout(function() {
            location.href = `${baseUrl}/?imsi=401018094158800`;
        }, 2000);
    }

    $("#resetUser").click( function() {
        $('#resetUserModal').modal('show');
    });

    CLoseModalReset = () => {
        $('#resetUserModal').modal('hide');
    }

    ShowUpdateProfile = (type) => {
        $('#updateProfileModal').modal('show');

        if (type == "fullname") {
            val = $("#text-fullname").text();
            $(".label-text").html("Fullname");
            $("#valueUpdate").val(val);
            $("#typeInput").val("fullname");
        }

        if (type == "phone") {
            val = $("#text-phone").text();
            $(".label-text").html("Personal Phone Number");
            $("#valueUpdate").val(val);
            $("#typeInput").val("phone");
        }

        if (type == "email") {
            val = $("#text-email").text();
            $(".label-text").html("E-mail Adress");
            $("#valueUpdate").val(val);
            $("#typeInput").val("email");
        }
    }

    UpdateProfile = () => {
        var value = $("#valueUpdate").val();
        var type =  $("#typeInput").val();

        if (type == "fullname") { 
            $(".text-fullname").html(value);
        }

        if (type == "phone") { 
            $(".text-phone").html(value);
        }

        if (type == "email") { 
            $(".text-email").html(value);
        }

        $('#messageUpdateprofile').modal('show');
        $('#updateProfileModal').modal('hide');
    }
</script>