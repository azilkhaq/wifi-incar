<script>
    var ctx = document.getElementById('myChart');

    var myChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [10, 30],
                backgroundColor: [
                    '#A41115',
                    'white',
                ],
                borderColor: [
                    '#A41115',
                    'white',
                ],
                borderWidth: 1,
                cutout: 55,
                radius: 85,
                borderRadius: 5,
            }]
        },
        options: {
            events: [],
        }
    });
</script>