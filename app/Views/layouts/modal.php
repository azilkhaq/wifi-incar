<!-- messageSuccess -->
<div class="modal fade dialogbox" id="messageSuccess" data-bs-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <img src="<?= base_url("public/assets/img/success.svg") ?>" alt="image" class="image-success">
            <div class="modal-header">
                <h5 class="modal-title">Payment successful.</h5>
            </div>
            <div class="modal-body">
                <span id="successMessage"></span>

                <a href="<?= base_url("home") ?>" class="btn btn-primary rounded">Close</a>
            </div>
        </div>
    </div>
</div>
<!-- * messageSuccess -->

<div class="modal fade dialogbox" id="DialogBasic" data-bs-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            </div>
            <div class="modal-body">
                Buy 5GB Rp 25,000 with Go-Pay
            </div>
            <div class="modal-footer">
                <div class="btn-inline">
                    <a href="#" class="btn btn-text-secondary" data-bs-dismiss="modal">Cancel</a>
                    <a type="button" class="btn btn-text-primary" onclick="Success()">Confirm</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Login -->
<div class="modal fade dialogbox" id="loginMessage" data-bs-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <img src="<?= base_url("public/assets/img/wrong.svg") ?>" alt="image" class="image-success">
            <div class="modal-header">
                <h5 class="modal-title">Incorrect e-mail address or password. Please try again.</h5>
            </div>
            <div class="modal-body">
                <span id="successMessage"></span>

                <button data-bs-dismiss="modal" class="btn btn-primary rounded">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- * Login -->

<!-- registerSuccess -->
<div class="modal fade dialogbox" id="registerSuccess" data-bs-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <img src="<?= base_url("public/assets/img/success.svg") ?>" alt="image" class="image-success">
            <div class="modal-header">
                <h5 class="modal-title">Registration successfuly.</h5>
            </div>
            <div class="modal-body">
                <span id="successMessage"></span>

                <a href="<?= base_url("home") ?>" class="btn btn-primary rounded">Close</a>
            </div>
        </div>
    </div>
</div>
<!-- * registerSuccess -->

<div class="modal fade dialogbox" id="confirmModal" data-bs-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            </div>
            <input type="text" id="provider" hidden>
            <input type="text" id="productId" hidden>
            <div class="modal-body">
                <span id="productName"></span>
            </div>
            <div class="modal-footer">
                <div class="btn-inline">
                    <a href="#" class="btn btn-text-secondary" data-bs-dismiss="modal">Cancel</a>
                    <a type="button" class="btn btn-text-primary" onclick="NextConfirm()">Confirm</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade dialogbox" id="paymentFailed" data-bs-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <img src="<?= base_url("public/assets/img/wrong.svg") ?>" alt="image" class="image-success">
            <div class="modal-header">
                <h5 class="modal-title">Payment failed.</h5>
            </div>
            <div class="modal-body">
                <span id="successMessage"></span>

                <button data-bs-dismiss="modal" class="btn btn-primary rounded">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade dialogbox" id="messageSuccessReset" data-bs-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <img src="<?= base_url("public/assets/img/success.svg") ?>" alt="image" class="image-success">
            <div class="modal-header">
                <h5 class="modal-title">Your password has been successfully reset.</h5>
            </div>
            <div class="modal-body">
                <span id="successMessage"></span>

                <a href="<?= base_url("/") ?>" class="btn btn-primary rounded">Close</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade dialogbox" id="resetUserModal" data-bs-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" style="font-size: 14px;">You are about to replace current existing user for this car. This action cannot be undone. Are you sure to the delete the provius profile?</h6>
            </div>
            <div class="modal-body">
                <span id="successMessage"></span>

                <a href="<?= base_url("auth/register") ?>" class="btn btn- rounded">Reset</a>
                <a onclick="CLoseModalReset()" class="btn btn-primary rounded">Cancel</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade dialogbox" id="updateProfileModal" data-bs-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            </div>
            <div class="modal-body">
                <label class="label-text"></label>
                <input type="text" class="form-control" id="valueUpdate">
                <input type="text" class="form-control" id="typeInput" hidden>
            </div>
            <div class="modal-footer">
                <div class="btn-inline">
                    <a href="#" class="btn btn-text-secondary" data-bs-dismiss="modal">Cancel</a>
                    <a type="button" class="btn btn-text-primary" onclick="UpdateProfile()">Update</a>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade dialogbox" id="messageUpdateprofile" data-bs-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <img src="<?= base_url("public/assets/img/success.svg") ?>" alt="image" class="image-success">
            <div class="modal-header">
                <h5 class="modal-title">Profile changed successfully.</h5>
            </div>
            <div class="modal-body">
                <span id="successMessage"></span>

                <a href="#" data-bs-dismiss="modal" class="btn btn-primary rounded">Close</a>
            </div>
        </div>
    </div>
</div>
