<div class="appBottomMenu infinite-menu">
    <div class="menu-home">
        <div class="card card-menu-home">
            <div class="card-body">
                <div class="content">

                    <div class="top-menu">
                        <div class="card card-top-menu">
                            <div class="card-body">
                                <div style="display: flex;">
                                    <a class="btn btn-menu-icon"><img src="<?= base_url("public/assets/img/icon1.svg") ?>" alt="image"></a><span class="span-menu-icon">Connectivity</span>&emsp;
                                    <a href="<?= base_url("product") ?>" class="btn btn-menu-icon"><img src="<?= base_url("public/assets/img/icon2.svg") ?>" alt="image"></a><span class="span-menu-icon2">Purchase</span>&emsp;
                                    <a href="<?= base_url("support") ?>" class="btn btn-menu-icon"><img src="<?= base_url("public/assets/img/support.svg") ?>" style="width: 30px; height:31px;" alt="image"></a><span class="span-menu-icon3">Support</span>&emsp;
                                    <a href="<?= base_url("profile") ?>" class="btn btn-menu-icon"><img src="<?= base_url("public/assets/img/icon3.svg") ?>" alt="image"></a><span class="span-menu-icon4">Profile</span>&emsp;
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>