<script src="<?= base_url('public/assets/js/lib/bootstrap.min.js') ?>"></script>
<script type="module" src="https://unpkg.com/ionicons@5.4.0/dist/ionicons/ionicons.js"></script>
<script src="<?= base_url('public/assets/js/plugins/splide/splide.min.js') ?>"></script>
<script src="<?= base_url('public/assets/js/plugins/progressbar-js/progressbar.min.js') ?>"></script>
<script src="<?= base_url('public/assets/js/base.js') ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://apps.sistemit.com/tutorial/html2canvas/jquery.min.js"></script>
<script src="https://apps.sistemit.com/tutorial/html2canvas/html2canvas.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.0/chart.min.js"></script>
<script>
    const baseUrl = '<?= base_url(); ?>';
</script>