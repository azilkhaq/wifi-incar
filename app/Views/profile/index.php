<?= view('layouts/header'); ?>

<style>
    .menu-home {
        padding-top: 0px;
    }
</style>

<body>

    <div class="appHeader no-border transparent position-absolute">
        <div class="left">
            <a href="<?= base_url("home") ?>" class="headerButton goBack">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle"></div>
        <div class="right"></div>
    </div>

    <div id="appCapsule">

        <div class="menu-product">
            <div class="content-product">

                <div class="product" style="padding-bottom: 250px;">

                    <center>
                        <div class="numberCircle">KW</div>
                        <h5 style="padding-top: 10px;"><a href="<?= base_url("profile/forgot-password") ?>" style="color:black;">Change Password</a></h5>
                    </center>

                    <div class="top-product">

                        <div class="card card-top-history" style="border-radius: 10px; background: #ff9e9e;">
                            <div class="card-body" style="padding: 12px 15px;">
                                <div style="display: flex;">
                                    <div class="description-flex">
                                        <p class="desc-history desc-bold">Fullname</p>
                                        <p class="desc-history text-muted text-fullname" id="text-fullname">Kristanto Robby Winner</p>
                                    </div>
                                    <div class="a-class">
                                        <a type="button" onclick="ShowUpdateProfile('fullname')" style="color:black; font-size: 14px;">Change</a>
                                    </div>
                                </div>
                            </div>
                        </div><br>

                        <div class="card card-top-history" style="border-radius: 10px; background: #ff9e9e;">
                            <div class="card-body" style="padding: 12px 15px;">
                                <div style="display: flex;">
                                    <div class="description-flex">
                                        <p class="desc-history desc-bold">E-mail Address</p>
                                        <p class="desc-history text-muted text-email" id="text-email">kristantorobbywinner@gmail.com</p>
                                    </div>
                                    <a type="button" onclick="ShowUpdateProfile('email')" style="color:black; font-size: 14px;">Change</a>
                                </div>
                            </div>
                        </div><br>

                        <div class="card card-top-history" style="border-radius: 10px ; background: #ff9e9e;">
                            <div class="card-body" style="padding: 12px 15px;">
                                <div style="display: flex;">
                                    <div class="description-flex">
                                        <p class="desc-history desc-bold">Personal Phone Number</p>
                                        <p class="desc-history text-muted text-phone" id="text-phone">+62812345678</p>
                                    </div>
                                    <a type="button" onclick="ShowUpdateProfile('phone')" style="color:black; font-size: 14px;">Change</a>
                                </div>
                            </div>
                        </div><br>

                        <div style="float: right;">
                            <i style="position: absolute;color: white;margin-left: 12px;margin-top: 6px;font-size: 16px;">
                                <ion-icon name="trash-outline"></ion-icon>
                            </i>&nbsp;<a type="button" id="resetUser" class="btn btn-primary rounded me-1" style="height: 34px !important;width: 147px;">Delete Account</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?= view('layouts/bottomMenu'); ?>

    </div>

</body>

<?= view('layouts/modal'); ?>
<?= view('layouts/script'); ?>
<?= view('js/auth'); ?>
<?= view('modal/popup'); ?>