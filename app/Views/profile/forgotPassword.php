<?= view('layouts/header'); ?>

<div id="appCapsule" class="pt-0">

    <div class="login-form mt-1">

        <div class="heading-login">
            <h5>Please enter your e-mail / phone number</h5>
        </div>

        <div class="section mt-1 mb-5" style="padding-top: 10px;">
            <form>
                <div class="form-group boxed">
                    <div class="input-wrapper">
                        <input type="text" class="form-control icon-rtl" id="email" placeholder="e-mail address / phone number" style="font-size: 12px;">
                        <i class="right-inner" style="top: 155px;">
                            <ion-icon name="person-circle-outline"></ion-icon>
                        </i>
                    </div>
                </div>

                <div class="btn-reset">
                    <a href="<?= base_url("profile") ?>" id="reset" class="btn btn rounded me-1">Back</a>
                    <a href="<?= base_url("profile/forgot-password-otp") ?>" id="reset" class="btn btn-primary rounded me-1">Reset</a>
                </div>
            </form>
        </div>
    </div>
</div>
</div>

<?= view('layouts/modal'); ?>
<?= view('layouts/script'); ?>
<?= view('js/auth'); ?>
<?= view('modal/popup'); ?>