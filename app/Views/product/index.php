<?= view('layouts/header'); ?>

<body>

    <div class="appHeader no-border transparent position-absolute">
        <div class="left">
            <a href="<?= base_url("home") ?>" class="headerButton goBack">
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle"></div>
        <div class="right">
        </div>
    </div>

    <div id="appCapsule">

        <div class="menu-product">
            <div class="content-product">

                <div class="product">
                    <h4>Choose Your Product</h4>

                    <div class="top-product">

                        <?php if ($product != NULL) : ?>
                            <?php foreach ($product as $row) : ?>
                                <div class="card card-top-history">
                                    <div class="card-body" style="padding: 12px 15px;">
                                        <div style="display: flex;">
                                            <div class="button-history">
                                                <a href="<?= base_url("confirm") . "/" . base64_encode($row->productId) ?>" class="btn btn-primary rounded me-1">Buy</a>
                                            </div>
                                            <div class="description">
                                                <p class="desc-history desc-bold"><?= $row->description->name ?></p>
                                                <p class="desc-history text-muted">Rp. <?= number_format($row->prices[0]->price) ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div><br>
                            <?php endforeach ?>
                        <?php endif ?>

                    </div>

                </div>
            </div>
        </div>

        <?= view('layouts/bottomMenu'); ?>

    </div>

</body>

<?= view('layouts/script'); ?>