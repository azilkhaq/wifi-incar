<?php

namespace Config;

$routes = Services::routes();
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
  require SYSTEMPATH . 'Config/Routes.php';
}

$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('LoginController');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

$routes->get('/', 'LoginController::index', ['namespace' => 'App\Controllers\Auth']);
$routes->get('otp', 'LoginController::otp', ['namespace' => 'App\Controllers\Auth']);

$routes->group('auth', function ($routes) {
  $routes->group('login', function ($routes) {
    $routes->post('post', 'LoginController::postLogin', ['namespace' => 'App\Controllers\Auth']);
  });

  $routes->group('otp', function ($routes) {
    $routes->post('post', 'LoginController::sendOTP', ['namespace' => 'App\Controllers\Auth']);
  });

  $routes->group('reset', function ($routes) {
    $routes->get('/', 'LoginController::reset', ['namespace' => 'App\Controllers\Auth']);
  });

  $routes->group('register', function ($routes) {
    $routes->get('/', 'LoginController::register', ['namespace' => 'App\Controllers\Auth']);
    $routes->post('post', 'LoginController::postRegister', ['namespace' => 'App\Controllers\Auth']);
  });

  $routes->group('forgot-password', function ($routes) {
    $routes->get('/', 'LoginController::forgotPassword', ['namespace' => 'App\Controllers\Auth']);
    $routes->get('otp', 'LoginController::otpForgotPassword', ['namespace' => 'App\Controllers\Auth']);
    $routes->get('new', 'LoginController::newForgotPassword', ['namespace' => 'App\Controllers\Auth']);
  });
});

$routes->group('profile', function ($routes) {
  $routes->get('/', 'ProfileController::index', ['namespace' => 'App\Controllers']);
  $routes->get('forgot-password', 'ProfileController::forgotPassword', ['namespace' => 'App\Controllers']);
  $routes->get('forgot-password-otp', 'ProfileController::otpForgotPassword', ['namespace' => 'App\Controllers']);
  $routes->get('forgot-password-new', 'ProfileController::newForgotPassword', ['namespace' => 'App\Controllers']);
});

$routes->group('support', function ($routes) {
  $routes->get('/', 'SupportController::index', ['namespace' => 'App\Controllers']);
});

$routes->group('product', function ($routes) {
  $routes->get('/', 'ProductController::index', ['namespace' => 'App\Controllers']);
  $routes->post('payment', 'ProductController::payment', ['namespace' => 'App\Controllers']);
});

$routes->get('home', 'HomeController::index', ['namespace' => 'App\Controllers']);
$routes->get('confirm/(:any)', 'HomeController::confirm/$1', ['namespace' => 'App\Controllers']);

if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
  require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
