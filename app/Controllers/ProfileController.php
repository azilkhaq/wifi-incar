<?php

namespace App\Controllers;

use App\Controllers\Base\BaseController;

class ProfileController extends BaseController
{
    public function index()
    {
        return view('profile/index');
    }

    public function forgotPassword()
    {
        return view("profile/forgotPassword");
    }

    public function otpForgotPassword()
    {
        return view("profile/otpForgotPassword");
    }

    public function newForgotPassword()
    {
        return view("profile/newForgotPassword");
    }
}
