<?php

namespace App\Controllers\Auth;

use App\Controllers\Base\BaseController;
use Config\Services;

class LoginController extends BaseController
{
    public function index()
    {
        $request = Services::request();

        $imsi = $request->getGet('imsi');
        $checkImsi = $imsi != null ? $imsi : "null";

        $result = curlHelper('https://wicardemo.nuncorp.id/api/imsi/' . $checkImsi, 'GET');

        $status = $result->status;

        if ($status == false) {
            return view("auth/register");
        } else {
            return view("auth/login");
        }
    }

    public function reset()
    {
        return view("auth/reset");
    }

    public function otp()
    {
        return view("auth/otp");
    }

    public function register()
    {
        return view("auth/register");
    }

    public function postRegister()
    {
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $request = Services::request();

        $fullname = $request->getPost('fullname');
        $phone = $request->getPost('phone');
        $email = $request->getPost('email');
        $carId = $request->getPost('carId');
        $identityNumber = $request->getPost('identityNumber');
        $password = $request->getPost('password');

        $body = [
            'msisdn' => $phone,
            'fullname' => $fullname,
            'emailAddress' => $email,
            'identityNumber' => $identityNumber,
            'password' => $password,
            'carId' => $carId
        ];

        $url = getenv('API_URL') . '/user/v1/register';

        $req = $client->post(
            $url,
            [
                'body' => json_encode($body),
                'headers' =>  [
                    'Content-type'        => 'application/json',
                ]
            ]
        );
    }

    public function postLogin()
    {
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $request = Services::request();
        $session = Services::session();

        $username = $request->getPost('username');
        $password = $request->getPost('password');

        $body = [
            'username' => $username,
            'password' => $password,
            'otp' => '123'
        ];

        $url = getenv('API_URL') . '/auth/v1/login';

        $req = $client->post(
            $url,
            [
                'body' => json_encode($body),
                'headers' =>  [
                    'Content-type'        => 'application/json',
                ]
            ]
        );

        $response = $req->getBody()->getContents();
        $result = json_decode($response);

        if ($result->code == 0) {
            $data = array();
            $data["token"] = $result->body->accessToken;
            $data["msisdn"] = $result->body->subscriber->msisdn;
            $data["carId"] = $result->body->subscriber->carId;
            $data["imsi"] = $result->body->subscriber->imsi;
            $data["emailAddress"] = $result->body->subscriber->emailAddress;
            $data["fullname"] = $result->body->subscriber->fullname;
            $data["identityNumber"] = $result->body->subscriber->identityNumber;
            $data["authenticated"] = true;
            $session->set($data);

            $url = 'https://wicardemo.nuncorp.id/api/generate/email';

            $req = $client->post(
                $url,
                [
                    'headers' =>  [
                        'Authorization' => $username
                    ]
                ]
            );
        }
    }

    public function  sendOTP()
    {
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $request = Services::request();
        $session = Services::session();

        $code = $request->getPost('code');

        $body = [
            'otp' => $code,
        ];

        $url = 'https://wicardemo.nuncorp.id/api/validate';

        $req = $client->post(
            $url,
            [
                'body' => json_encode($body),
                'headers' =>  [
                    'Authorization' => $session->get('msisdn'),
                ]
            ]
        );

        $response = $req->getBody()->getContents();
        $result = json_decode($response);

        if ($result->status == false) {
            return json_encode([
                "status" =>  false,
                "message" => $result->remark
            ]);
        } else {
            return json_encode([
                "status" =>  true,
                "message" => "Successfuly"
            ]);
        }
    }

    public function forgotPassword()
    {
        return view("auth/forgotPassword");
    }

    public function otpForgotPassword()
    {
        return view("auth/otpForgotPassword");
    }

    public function newForgotPassword()
    {
        return view("auth/newForgotPassword");
    }
}
