<?php

namespace App\Controllers;

use App\Controllers\Base\BaseController;

class SupportController extends BaseController
{
    public function index()
    {
        return view('support/index');
    }
}
