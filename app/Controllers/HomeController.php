<?php

namespace App\Controllers;

use App\Controllers\Base\BaseController;

class HomeController extends BaseController
{
    public function index()
    {
        return view('home/index');
    }

    public function product()
    {
        return view('home/product');
    }

    public function confirm($productId)
    {
        $productId = base64_decode($productId);

        $result = curlHelper(getenv('API_URL') . '/api/v1/product/fetch/' . $productId, 'GET');

        $data['product'] = $result->body;

        return view('home/confirm', $data);
    }
}
