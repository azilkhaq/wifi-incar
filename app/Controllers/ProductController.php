<?php

namespace App\Controllers;

use App\Controllers\Base\BaseController;
use Config\Services;

class ProductController extends BaseController
{
    public function index()
    {
        $result = curlHelper(getenv('API_URL') . '/api/v1/product/filter', 'GET');

        $data['product'] = $result->body;

        return view('product/index', $data);
    }

    public function payment()
    {
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $request = Services::request();
        $session = Services::session();

        $productId = $request->getPost('productId');
        $provider = $request->getPost('provider');

        $result = curlHelper(getenv('API_URL') . '/api/v1/product/fetch/' . $productId, 'GET');
        $data = $result->body;

        $body = [
            'payment_info' => [
                'payment_provider' => $provider,
                'payment_id' => '',
                'bank_name' => '',
                'installment_term' => '',
                'expiry_duration' => 5000,
                'duration_unit' => 'second'
            ],
            'item_details' => [
                'item_name' => $data->description->name,
                'item_price' => $data->prices[0]->price,
                'item_fee' => $data->freePurchases,
                'item_type' => 'recharge',
                'item_qty' => 1,
                'item_promoCode' => '',
                'currency' => 'IDR'
            ],
            'customer_details' => [
                'cust_phone' => $session->get('msisdn'),
                'cust_name' => $session->get('fullname'),
                'cust_email' => $session->get('emailAddress'),
                'cust_interface' => 'app'
            ],
        ];

        $url = 'https://wicardemo.nuncorp.id/api/payment';

        $req = $client->post(
            $url,
            [
                'body' => json_encode($body),
                'headers' =>  [
                    'Authorization' => $session->get('token'),
                    'Content-type'        => 'application/json',
                    'postman' => 'sembarang'
                ]
            ]
        );

        $response = $req->getBody()->getContents();
        $result = json_decode($response);

        if ($result->status_code == "00") {
            return json_encode([
                "status" =>  true,
                "message" => "Successfuly"
            ]);
        } else {
            return json_encode([
                "status" =>  false,
                "message" => $result->status_desc
            ]);
        }
    }
}
